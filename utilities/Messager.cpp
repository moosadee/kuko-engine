#include "Messager.hpp"

#include "Logger.hpp"

namespace kuko
{

std::map<std::string, std::string> Messager::msg;

//! Set a global message
/**
@param      key     Identifying key of the message
@param      value   Value of the message
*/
void Messager::Set( const std::string& key, const std::string& value )
{
    Logger::Out( "Set \"" + key + "\" = \"" + value + "\"", "Messager::Set" );
    Messager::msg[ key ] = value;
}

//! Retrieve a global message
/**
@param      key     Identifying key of the message
@return             The value of the message
*/
std::string Messager::Get( const std::string& key )
{
    return Messager::msg[ key ];
}

}
