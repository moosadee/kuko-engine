#ifndef MESSAGER_HPP
#define MESSAGER_HPP

#include <string>
#include <map>

namespace kuko
{

//! Simple message passer
class Messager
{
    public:
    static void Set( const std::string& key, const std::string& value );
    static std::string Get( const std::string& key );

    private:
    static std::map<std::string, std::string> msg;
};

}

#endif
