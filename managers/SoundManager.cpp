// Kuko Framework - https://github.com/Rejcx/Kuko - Rachel J. Morris - MIT License
#include "SoundManager.hpp"

#include "../utilities/Logger.hpp"
#include "../utilities/StringUtil.hpp"

namespace kuko
{

std::map<std::string, Mix_Chunk*> SoundManager::m_sounds;
std::map<std::string, Mix_Music*> SoundManager::m_music;

int SoundManager::m_musicVolume;
int SoundManager::m_soundVolume;

//! Clean up the Sound Manager audio
void SoundManager::Cleanup()
{
    ClearAudio();
}

//! Add a music file to the manager
/**
@param  key     Identifying key of the music
@param  path    Path to the music file to be added
*/
void SoundManager::AddMusic( const std::string& key, const std::string& path )
{
    Logger::Out( "Load music \"" + path + "\"", "SoundManager::AddMusic" );
    m_music.insert( std::pair< std::string, Mix_Music* >( key, LoadMusicFile( path ) ) );
}

//! Add a sound file to the manager
/**
@param  key     Identifying key of the sound
@param  path    Path to the sound file to be added
*/
void SoundManager::AddSound( const std::string& key, const std::string& path )
{
    Logger::Out( "Load sound \"" + path + "\"", "SoundManager::AddSound" );
    // TODO Possibly stores NULL, maybe fix this.
    m_sounds.insert( std::pair< std::string, Mix_Chunk* >( key, LoadSoundFile( path ) ) );
}

//! Clear out all the audio files currently loaded
void SoundManager::ClearAudio()
{
    Logger::Out( "SoundManager::Cleanup" );

    for (   std::map<std::string, Mix_Chunk*>::iterator it = m_sounds.begin();
            it != m_sounds.end();
            ++it )
    {
        if ( it->second != NULL )
        {
            Mix_FreeChunk( it->second );
            it->second = NULL;
        }
    }

    for (   std::map<std::string, Mix_Music*>::iterator it = m_music.begin();
            it != m_music.end();
            ++it )
    {
        if ( it->second != NULL )
        {
            Mix_FreeMusic( it->second );
            it->second = NULL;
        }
    }
}

//! Play a music file
/**
@param  key     The identifying key of the music file
@param  loop    Whether or not to loop the music automatically
*/
void SoundManager::PlayMusic( const std::string& key, bool loop )
{
    if ( Mix_PlayingMusic() != 0 )
    {
        Mix_HaltMusic();
    }

    if ( m_music.find( key ) != m_music.end() )
    {
        // -1 = infinite, 0 = none, # = how many times to loop
        int loops = ( loop ) ? -1 : 1;
        Mix_FadeInMusicPos( m_music[ key ], loops, 2000, 0 );
    }
    else
    {
        Logger::Out( "Music \"" + key + "\" not found" );
    }
}

//! Play a sound file
/**
@param  key     The identifying key of the sound file
*/
void SoundManager::PlaySound( const std::string& key )
{
    if ( m_sounds.find( key ) != m_sounds.end() )
    {
        // int Mix_PlayChannel(int channel, Mix_Chunk *chunk, int loops)
        Mix_PlayChannel( -1, m_sounds[ key ], 0 );
    }
}

//! Loads a sound file and returns the Mix_Chunk pointer
/**
@param  path        Path of the sound file
@return             The loaded Mix_Chunk item pointer
*/
Mix_Chunk* SoundManager::LoadSoundFile( const std::string& path )
{
    Mix_Chunk* sound = Mix_LoadWAV( path.c_str() );
    if( sound == NULL )
    {
        Logger::Error( "Error loading audio file \"" + path + "\": " + Mix_GetError() );
    }
    return sound;
}

//! Loads a music file and returns the Mix_Music pointer
/**
@param  path        Path of the music file
@return             The loaded Mix_Music item pointer
*/
Mix_Music* SoundManager::LoadMusicFile( const std::string& path )
{
    Mix_Music* music = Mix_LoadMUS( path.c_str() );
    if( music == NULL )
    {
        Logger::Error( "Error loading music file \"" + path + "\": " + Mix_GetError() );
    }
    return music;
}

//! Set the volume of music files
/**
@param  value       New volume for the music
*/
void SoundManager::SetMusicVolume( int value )
{
    m_musicVolume = value;
    Mix_VolumeMusic( m_musicVolume );
}

//! Set the volume of sound files
/**
@param  value       New volume for the sound
*/
void SoundManager::SetSoundVolume( int value )
{
    m_soundVolume = value;
    Mix_Volume( -1, m_soundVolume );
}

}
