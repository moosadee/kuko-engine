#include "LuaLanguage.hpp"

#ifndef NOLUA

#include "../LuaManager.hpp"
#include "../../utilities/Logger.hpp"
#include "../../utilities/StringUtil.hpp"

namespace kuko
{

LuaLanguage::LuaLanguage()
{
}

LuaLanguage::~LuaLanguage()
{
}

//! Add a language to the program's languages
/**
@param  key     The unique identifier of the language
@param  path    The path to the language file
*/
bool LuaLanguage::AddLanguage( const std::string& key, const std::string& path )
{
    m_currentLanguage = key;
    return kuko::LuaManager::LoadScript( path );
}

//! Retrieve the current language's identifier
/**
@param          Returns the key of the current language in use
*/
std::string LuaLanguage::CurrentLanguage()
{
    return m_currentLanguage;
}

//! Retrieve the localized text associated with some key
/**
@param  key     The unique identifier of a text string to retrieve
@return         The localized string for that key, or NOTFOUND if not found
*/
std::string LuaLanguage::Text( const std::string& key )
{
    std::string value = kuko::LuaManager::Language_GetText( key );

    if ( value == "NOTFOUND" )
    {
        Logger::Error( "Value for key \"" + key + "\" not found in Lua script for language \"" + m_currentLanguage + "\"", "LuaLanguage::Text" );
    }

    return ( value == "NOTFOUND" ) ? key + " NOT FOUND" : value;
}

//! Retrieve the localized text associated with some key
/**
@param  langType    The specific language to load this string from
@param  key         The unique identifier of a text string to retrieve
@return             The localized string for that key, or NOTFOUND if not found
*/
std::string LuaLanguage::Text( const std::string& langType, const std::string& key )
{
    std::string value = kuko::LuaManager::Language_GetText( langType, key );

    if ( value == "NOTFOUND" )
    {
        Logger::Error( "Value for key \"" + key + "\", langType \"" + langType + "\" not found in Lua script for language \"" + m_currentLanguage + "\"", "LuaLanguage::Text" );
    }


    return ( value == "NOTFOUND" ) ? key + " NOT FOUND" : value;
}

//! Get the suggested font file for this language
/**
@return     The suggested font to be used for the current language
*/
std::string LuaLanguage::GetSuggestedFont()
{
    std::string suggested = kuko::LuaManager::Language_GetSuggestedFont();
    Logger::Out( "Suggested font: " + suggested );
    return suggested;
}

std::string LuaLanguage::SpecialField( const std::string& langType, const std::string& field, const std::string& key )
{
    std::string value = kuko::LuaManager::Language_GetSpecialField( langType, field, key );

    if ( value == "NOTFOUND" )
    {
        Logger::Error( "Value for key \"" + key + "\", field \"" + field + "\", langType \"" + langType + "\" not found in Lua script for language \"" + m_currentLanguage + "\"", "LuaLanguage::SpecialField" );
    }

    return ( value == "NOTFOUND" ) ? key + " NOT FOUND" : value;
}

std::string LuaLanguage::SpecialField( const std::string& langType, const std::string& field, int key )
{
    std::string value = kuko::LuaManager::Language_GetSpecialField( langType, field, key );

    if ( value == "NOTFOUND" )
    {
        Logger::Error( "Value for key \"" + StringUtil::IntToString( key ) + "\", field \"" + field + "\", langType \"" + langType + "\" not found in Lua script for language \"" + m_currentLanguage + "\"", "LuaLanguage::SpecialField" );
    }

    return ( value == "NOTFOUND" ) ? key + " NOT FOUND" : value;
}

int LuaLanguage::SpecialFieldCount( const std::string& langType, const std::string& field )
{
    int value = kuko::LuaManager::Language_GetSpecialFieldCount( langType, field );
    return value;
}

}

#endif
