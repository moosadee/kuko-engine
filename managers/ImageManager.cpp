// Kuko Framework - https://github.com/Rejcx/Kuko - Rachel J. Morris - MIT License
#include "ImageManager.hpp"

#include "../base/Application.hpp"

namespace kuko
{

std::map<std::string, SDL_Texture*> ImageManager::m_textures;
std::map<std::string, std::string> ImageManager::m_filenames;

//! Load and add a new texture
/**
@param  key     The unique identifier of the new texture
@param  path    The file path of the texture
*/
void ImageManager::AddTexture( const std::string& key, const std::string& path )
{
    m_filenames.insert( std::pair<std::string, std::string>( key, path ) );
    m_textures.insert( std::pair<std::string, SDL_Texture*>( key, LoadFile( path ) ) );
}

//! Add a new texture that was loaded externally, usually used with the MenuManager
/**
@param  key                 The unique identifier of the new texture
@param  externalTexture     A texture loaded elseware to add into the manager
*/
void ImageManager::AddTexture( const std::string& key, SDL_Texture* externalTexture )
{
    Logger::Out( "Add externally-created texture \"" + key + "\"", "ImageManager::AddTexture" );
    m_textures.insert( std::pair<std::string, SDL_Texture*>( key, externalTexture ) );
}

//! Destroy a texture that is no longer desired
/**
@param  ptrTexture      Pointer to the SDL_Texture to destroy
*/
void ImageManager::DestroyTexture( SDL_Texture* ptrTexture )
{
    // Find texture
    std::string removeId = "";
    for ( std::map<std::string, SDL_Texture*>::iterator it = m_textures.begin();
        it != m_textures.end(); it++ )
    {
        if ( it->second == ptrTexture )
        {
            Logger::Out( "Request to destroy texture \"" + it->first + "\"", "ImageManager::AddTexture" );
            SDL_DestroyTexture( it->second );
            it->second = NULL;
            removeId = it->first;
            break;
        }
    }

    if ( removeId != "" )
    {
        m_textures.erase( removeId );
    }
}

//! Free all textures currently stored in the ImageManager
void ImageManager::DestroyAllTextures()
{
    for ( std::map<std::string, SDL_Texture*>::iterator it = m_textures.begin();
        it != m_textures.end(); it++ )
    {
        Logger::Out( "Request to destroy texture \"" + it->first + "\"", "ImageManager::AddTexture" );
        SDL_DestroyTexture( it->second );
        it->second = NULL;
        break;
    }
}

//! Clear out all loaded textures and associated filenames currently stored in the Image Manager
void ImageManager::ClearTextures()
{
    DestroyAllTextures();
    m_textures.clear();
    m_filenames.clear();
}

//! Load an image file
/**
@param  path        Path to the image file to load
@return             The loaded SDL_Texture* item
*/
SDL_Texture* ImageManager::LoadFile( const std::string& path )
{
    SDL_Surface* loaded = IMG_Load( path.c_str() );
    if ( loaded == NULL )
    {
        Logger::Error( "Error loading image \"" + path + "\"" );
    }

    SDL_Texture* texture = SDL_CreateTextureFromSurface( Application::GetRenderer(), loaded );
    if ( texture == NULL )
    {
        Logger::Error( "Error optimizing image \"" + path + "\"", "ImageManager::LoadFile" );
    }

    SDL_FreeSurface( loaded );
    return texture;
}

//! Return the dimensions of a texture, given some key
/**
@param          key         The unique identifier of the texture to get the dimensions of
@param[out]     w           The width of the texture is stored in this pointer
@param[out]     h           The height of the texture is stored in this pointer
*/
void ImageManager::GetTextureDimensions( const std::string& key, int* w, int* h )
{
    *w = 0;
    *h = 0;

    SDL_Texture* texture = GetTexture( key );

    if ( texture == NULL )
    {
        Logger::Error( "Error: Cannot get dimensions for NULL texture!", "ImageManager::GetTextureDimensions" );
        return;
    }

    if ( SDL_QueryTexture( texture, NULL, NULL, w, h ) != 0 )
    {
        Logger::Error( "Error: Failed to query texture information", "ImageManager::GetTextureDimensions" );
    }
}

//! Retrieve a texture with the given key
/**
@param  key     The unique identifier of the texture to retrieve
@return         The associated SDL_Texture*, or NULL if not found
*/
SDL_Texture* ImageManager::GetTexture( const std::string& key )
{
    if ( m_textures[ key ] == NULL )
    {
        Logger::Error( "Error - could not find image \"" + key + "\"", "ImageManager::GetTexture" );
    }
    return m_textures[ key ];
}

//! Retrieve the filename of a texture with a given key
/**
@param  key     The unique identifier of the texture
@return         The filename of the texture, or an empty string if not found
*/
std::string ImageManager::GetTextureFile( const std::string& key )
{
    if ( m_filenames[ key ] == "" )
    {
        Logger::Error( "Error - could not find image \"" + key + "\"", "ImageManager::GetTextureFile" );
    }
    return m_filenames[ key ];
}

//! Render a texture with text
/**
@param  label       The text to render to the texture
@param  textColor   The text color to draw
@param  font        Pointer to the font to draw this text as
@return             A SDL_Texture* item with text drawn to it
*/
SDL_Texture* ImageManager::GenerateLabelTexture( const std::string& label, SDL_Color textColor, TTF_Font* font )
{
    if ( font == NULL )
    {
        Logger::Error( "Error: Font for label \"" + label + "\" is NULL!", "ImageManager::GenerateLabelTexture" );
        return NULL;
    }

    SDL_Surface* textSurface = TTF_RenderUTF8_Solid( font, label.c_str(), textColor );
    SDL_Texture* newTexture = SDL_CreateTextureFromSurface( kuko::Application::GetRenderer(), textSurface );
    ImageManager::AddTexture( "genlabel-" + label, newTexture );
    SDL_FreeSurface( textSurface );
    return GetTexture( "genlabel-" + label );
}

//! Clean up all the fonts
void ImageManager::Cleanup()
{
    for (   std::map<std::string, SDL_Texture*>::iterator it = m_textures.begin();
            it != m_textures.end();
            ++it )
    {
        if ( it->second != NULL )
        {
            SDL_DestroyTexture( it->second );
            it->second = NULL;
        }
    }
}

//void ImageManager::Draw( const Sprite& sprite )
//{
//    SDL_RendererFlip flip = ( sprite.isFlipped ) ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE;
//
//    SDL_Rect rect = sprite.position.ToSDLRect();
//    SDL_Rect frame = sprite.frame.ToSDLRect();
//
//    SDL_RenderCopyEx(
//        kuko::Application::GetRenderer(),
//        sprite.texture,
//        &frame,
//        &rect,
//        sprite.angle,
//        NULL,
//        flip
//    );
//}

//! Draw a texture to the screen
/**
@param      ptrTexture          A pointer to the texture to draw
@param      ptrRect             An SDL_Rect* of the position and dimensions with which to draw the texture to the screen
@param      ptrFrame            An SDL_Rect* of the position and dimensions of a frame on the spritesheet to draw
@param      flip                A SDL_RendererFlip with information on flipping the texture, as applicable
@param      angle               The ange at which the texture will be rotated
*/
void ImageManager::Draw( SDL_Texture* ptrTexture, SDL_Rect* ptrRect, SDL_Rect* ptrFrame, SDL_RendererFlip flip, float angle )
{
    SDL_RenderCopyEx(
        kuko::Application::GetRenderer(),
        ptrTexture,
        ptrFrame,
        ptrRect,
        angle,
        NULL,
        flip
    );
}

//! Draw a texture to the screen
/**
@param      ptrTexture          A pointer to the texture to draw
@param      x                   The x coordinate to render the texture at
@param      y                   The y coordinate to render the texture at
*/
void ImageManager::Draw( SDL_Texture* ptrTexture, int x, int y )
{
    SDL_Rect pos;
    pos.x = x;
    pos.y = y;
    pos.w = 200;
    pos.h = 50;

    SDL_RenderCopyEx(
        kuko::Application::GetRenderer(),
        ptrTexture,
        NULL,
        &pos,
        0.0f,
        NULL,
        SDL_FLIP_NONE
    );
}

//! Draw a texture to the screen
/**
@param      ptrTexture          A pointer to the texture to draw
@param      rect                The SDL_Rect with information on position and dimensions for the texture to be rendered
*/
void ImageManager::Draw( SDL_Texture* ptrTexture, SDL_Rect rect )
{
    SDL_RenderCopyEx(
        kuko::Application::GetRenderer(),
        ptrTexture,
        NULL,
        &rect,
        0.0f,
        NULL,
        SDL_FLIP_NONE
    );
}

//! Draw a rectangle primitive
/**
@param  pos         A FloatRect of the position and dimensions for the rectangle to be drawn
@param  r           The red color value (0-255)
@param  g           The green color value (0-255)
@param  b           The blue color value (0-255)
@param  thickness   The thickness of the draw line
@param  filledIn    Whether or not to fill in the rectangle or leave the inside transparent
*/
void ImageManager::DrawRectangle( kuko::FloatRect pos, int r, int g, int b, int thickness, bool filledIn )
{
    SDL_Rect rect = pos.ToSDLRect();
    SDL_SetRenderDrawColor( Application::GetRenderer(), r, g, b, 255 );

    if ( filledIn )
    {
        SDL_RenderFillRect( Application::GetRenderer(), &rect );
    }
    else
    {
        SDL_RenderDrawRect( Application::GetRenderer(), &rect );
    }

    for ( int i = 1; i < thickness; i++ )
    {
        rect.x -= 1;
        rect.y -= 1;
        rect.w += 2;
        rect.h += 2;
        SDL_RenderDrawRect( Application::GetRenderer(), &rect );
    }
}

}
