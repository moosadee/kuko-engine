#include "ConfigManager.hpp"

#include <fstream>

#include "LanguageManager.hpp"
#include "FontManager.hpp"

#include "../utilities/Logger.hpp"
#include "../utilities/StringUtil.hpp"

#ifdef NOLUA
#include "interfaces/LualessConfig.hpp"
#else
#include "interfaces/LuaConfig.hpp"
#endif

namespace kuko
{

IConfig* ConfigManager::m_config;

//! Initialize the config pointer to be LuaConfig or LualessConfig based on build flag NOLUA
void ConfigManager::Setup()
{
    m_config = NULL;
    #ifdef NOLUA
        Logger::Out( "Setup LualessConfig", "ConfigManager::Setup" );
        m_config = new LualessConfig;
    #else
        Logger::Out( "Setup LuaConfig", "ConfigManager::Setup" );
        m_config = new LuaConfig;
    #endif
}

//! Cleans up the config
void ConfigManager::Cleanup()
{
    if ( m_config != NULL )
    {
        delete m_config;
        m_config = NULL;
    }
}

//! Saves the current state of the config
void ConfigManager::SaveConfig()
{
    m_config->SaveConfig();
}

//! Loads the config, given the variable keys passed in
/**
@param	settings	List of keys to load in to the config from the file
@return			Returns true if loading was successful, or false if not.
*/
bool ConfigManager::LoadConfig( const std::vector<std::string>& settings )
{
    return m_config->LoadConfig( settings );
}

//! Set the value of a specific config option
/**
@param	key	The identifying key of the option
@param	val	The new value to store for this key
*/
void ConfigManager::SetOption( const std::string& key, const std::string& val )
{
    m_config->SetOption( key, val );
}

//! Get the current value stored for some config option
/**
@param	key	The unique identifier for the option
@return		The value stored for that key
*/
std::string ConfigManager::GetOption( const std::string& key )
{
    return m_config->GetOption( key );
}

//! Get an option, casting it to int before returning
/**
@param	key	The unique identifier for the option
*/
int ConfigManager::GetIntOption( const std::string& key )
{
    return StringUtil::StringToInt( GetOption( key ) );
}

//! Create a new save file for a player
/**
@param	playername	The name of the player (also used in the filename)
@param	settings	A key-value map of settings to store for this save file
*/
void ConfigManager::CreateNewSave( const std::string& playername, std::map<std::string, std::string>& settings )
{
    m_config->CreateNewSave( playername, settings );
}

//! Loads a save game state for a player
/**
@param	filename	The filename of the save file to load
@param	settings	A list of keys to load in options for from the file
@return			True if loading was a success, or false otherwise
*/
bool ConfigManager::LoadState( const std::string& filename, const std::vector<std::string>& settings )
{
    return m_config->LoadState( filename, settings );
}

//! Get a save option from the save file
/**
@param	key	The unique identifier of the save game option
@return		The string value of that option
*/
std::string ConfigManager::GetSaveData( const std::string& key )
{
    return m_config->GetSaveData( key );
}

//! Set a save file option value
/**
@param	key	The unique identifier of the save file option
@param	val	The new value to store in the save file under that key
*/
void ConfigManager::SetSaveData( const std::string& key, const std::string& val )
{
    m_config->SetSaveData( key, val );
}

//! Removes the current save file being used
void ConfigManager::DeleteCurrentSavefile()
{
    m_config->DeleteCurrentSavefile();
}

//! Gets the name of the current save file
/**
@return		The name of the current save game
*/
std::string ConfigManager::GetSavegameName()
{
    return m_config->GetSavegameName();
}

//! Get the amount of save game files
/**
@return		The amount of save game files; this number is stored in the main config file
*/
int ConfigManager::GetSavegameCount()
{
    return m_config->GetSavegameCount();
}

}
