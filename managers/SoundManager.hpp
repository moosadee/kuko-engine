// Kuko Framework - https://github.com/Rejcx/Kuko - Rachel J. Morris - MIT License
#ifndef _KUKO_SOUNDMANAGER
#define _KUKO_SOUNDMANAGER

#include <SDL.h>

#include <SDL_mixer.h>

#include <map>
#include <string>

namespace kuko
{

//! Manages loading, storing, and playing sounds/music files
class SoundManager
{
    public:
    static void Cleanup();

    static void AddMusic( const std::string& id, const std::string& path );
    static void AddSound( const std::string& id, const std::string& path );
    static void ClearAudio();

    static void PlayMusic( const std::string& key, bool loop );
    static void PlaySound( const std::string& key );

    static void SetMusicVolume( int value );
    static void SetSoundVolume( int value );

    protected:
    //! Key-value list of sounds
    static std::map<std::string, Mix_Chunk*> m_sounds;
   //! Key-value list of music
    static std::map<std::string, Mix_Music*> m_music;

    static Mix_Chunk* LoadSoundFile( const std::string& path );
    static Mix_Music* LoadMusicFile( const std::string& path );

    //! The music volume
    static int m_musicVolume;
    //! The sound volume
    static int m_soundVolume;
};

}

#endif
