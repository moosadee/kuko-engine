// Kuko Framework - https://github.com/Rejcx/Kuko - Rachel J. Morris - MIT License
#include "StateManager.hpp"

#include "../utilities/Logger.hpp"

namespace kuko
{

std::stack< std::string > StateManager::m_stateStack;

//! Initialize the State Manager
StateManager::StateManager()
{
    m_ptrCurrentState = NULL;
    m_isDone = false;
}

//! Clean up the State Manager
void StateManager::Cleanup()
{
    for ( std::map< std::string, IState* >::iterator iter = m_lstStates.begin();
            iter != m_lstStates.end();
            ++iter )
    {
        if ( iter->second != NULL )
        {
            delete iter->second;
            iter->second = NULL;
        }
    }
}

//! Return whether the current state is finished
/**
@return     True if the state is done, or false if not.
*/
bool StateManager::IsDone()
{
    return m_isDone;
}

//! Add a new state to the list of available states
/**
@param  title       The identifier key of this state
@param  ptrState    A pointer to a state, must be a child of the IState interface
*/
void StateManager::AddState( const std::string& title, IState* ptrState )
{
    m_lstStates.insert(
        std::pair< std::string, IState* >(
            title,
            ptrState
        ) );
}

//! Push a new state onto the view stack
/**
@param  key     The name of the state to push
*/
void StateManager::PushState( const std::string& key )
{
    Logger::Out( "Switch state to \"" + key + "\"", "StateManager::PushState" );
    // Clean the current state
    CleanCurrentState();

    // Initialize the next stat,
    m_ptrCurrentState = m_lstStates[ key ];
    if ( m_ptrCurrentState == NULL )
    {
        Logger::Error( "Error: Current state pointer is NULL!", "StateManager::PushState" );
        return;
    }
    m_ptrCurrentState->Setup();

    // Push this state on the stack
    m_stateStack.push( key );
}

//! Pop the top state in the view stack off the stack
void StateManager::PopState()
{
    m_stateStack.pop();
}

//! Get the name of the top-most state on the view stack
std::string StateManager::TopState()
{
    return m_stateStack.top();
}

//! Get the name of the top-most state on the view stack, and remove it
/**
@return         The identifying key of the state that was on the top of the stack
*/
std::string StateManager::PopAndGetTop()
{
    PopState();
    return TopState();
}

//! Get the amount of states currently on the view stack
/**
@return     The amount of states on the view stack
*/
int StateManager::GetStateStackSize()
{
    return m_stateStack.size();
}

//! Wrapper function to handle calling the state's Update function, and handling whether the state is done.
void StateManager::UpdateCurrentState()
{
    if ( m_ptrCurrentState != NULL )
    {
        m_ptrCurrentState->Update();

        if ( m_ptrCurrentState->IsDone() )
        {
            std::string nextState = m_ptrCurrentState->GetNextState();
            Logger::Out( "Received state change signal, go to: \"" + nextState + "\"", "StateManager::UpdateCurrentState" );

            if ( nextState == "quit" )
            {
                m_isDone = true;
            }
            else if ( m_lstStates[ nextState ] != NULL )
            {
                PushState( nextState );
            }
            else
            {
                Logger::Error( "Error switching states: could not find state: \"" + nextState + "\"", "StateManager::UpdateCurrentState" );
            }
        }
    }
}

//! Wrapper function to handle drawing the current state
void StateManager::DrawCurrentState()
{
    if ( m_ptrCurrentState != NULL )
    {
        m_ptrCurrentState->Draw();
    }
}

//! Clears the current state and prepares a new state to be active
void StateManager::CleanCurrentState()
{
    if ( m_ptrCurrentState != NULL )
    {
        m_ptrCurrentState->Cleanup();
        m_ptrCurrentState = NULL;
    }
}

}
