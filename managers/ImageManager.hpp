// Kuko Framework - https://github.com/Rejcx/Kuko - Rachel J. Morris - MIT License
#ifndef _KUKO_IMAGEMANAGER
#define _KUKO_IMAGEMANAGER

#include <SDL.h>
#include <SDL_image.h>

#include "SDL_ttf.h"

#include <map>
#include <string>

#include "../base/Sprite.hpp"

namespace kuko
{

class ImageManager
{
    public:
    static void Setup();
    static void Cleanup();

    static void AddTexture( const std::string& id, const std::string& path );
    static void AddTexture( const std::string& id, SDL_Texture* externalTexture );
    static void DestroyTexture( SDL_Texture* ptrTexture );
    static void DestroyAllTextures();
    static void ClearTextures();
    static SDL_Texture* GetTexture( const std::string& key );
    static std::string GetTextureFile( const std::string& key );
    static SDL_Texture* GenerateLabelTexture( const std::string& label, SDL_Color textColor, TTF_Font* font );

    static void GetTextureDimensions( const std::string& id, int* w, int* h );

    //static void Draw( const Sprite& sprite );
    static void Draw( SDL_Texture* ptrTexture, int x, int y );
    static void Draw( SDL_Texture* ptrTexture, SDL_Rect rect );
    static void Draw( SDL_Texture* ptrTexture, SDL_Rect* ptrRect, SDL_Rect* ptrFrame, SDL_RendererFlip flip, float angle );

    static void DrawRectangle( kuko::FloatRect pos, int r, int g, int b, int thickness = 1, bool filledIn = false );

    protected:
    //! A key-value list of the loaded textures
    static std::map<std::string, SDL_Texture*> m_textures;
    //! A key-value list of the images and their associated filenames
    static std::map<std::string, std::string> m_filenames;
    static SDL_Texture* LoadFile( const std::string& path );
};

}

#endif
