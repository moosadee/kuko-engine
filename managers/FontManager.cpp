// Kuko Framework - https://github.com/Rejcx/Kuko - Rachel J. Morris - MIT License
#include "FontManager.hpp"

#include "../utilities/Logger.hpp"

namespace kuko
{

std::map<std::string, TTF_Font*> FontManager::m_fonts;

void FontManager::Setup()
{
}

//! Clean up the font manager and related library
void FontManager::Cleanup()
{
    Logger::Out( "FontManager::Cleanup" );

    for (   std::map<std::string, TTF_Font*>::iterator it = m_fonts.begin();
            it != m_fonts.end();
            ++it )
    {
        if ( it->second != NULL )
        {
            TTF_CloseFont( it->second );
            it->second = NULL;
        }
    }
}

//! Load and add a new font
/**
@param  key     The unique identifier for the font
@param  path    The file path of the font file
@param  size    The font size to load the font for
*/
void FontManager::AddFont( const std::string& key, const std::string& path, int size )
{
    Logger::Out( "Add font \"" + key + "\" from path " + path, "FontManager::AddFont", "fonts" );
    m_fonts.insert( std::pair<std::string, TTF_Font*>( key, LoadFile( path, size ) ) );
}

//! Reload a font and replace it. This can be useful for multi-lingual support
/**
@param  key     The identifier of the font to replace
@param  path    Path of the new font to load
@param  size    The font size to load the font for
*/
void FontManager::ReplaceFont( const std::string& key, const std::string& path, int size )
{
    Logger::Out( "Replace font \"" + key + "\" with path " + path, "FontManager::ReplaceFont", "fonts" );

    if ( m_fonts[ key ] != NULL )
    {
        Logger::Out( "Remove font at ID" );
        TTF_CloseFont( m_fonts[ key ] );
        Logger::Out( "Insert new" );
        m_fonts[ key ] = LoadFile( path, size );
    }
}

//! Remove all fonts currently loaded
void FontManager::ClearFonts()
{
    m_fonts.clear();
}

//! Retrieve a font, given some key
/**
@param  key     The unique identifier of the font
@return         A TTF_Font* object
*/
TTF_Font* FontManager::GetFont( const std::string& key )
{
    if ( m_fonts.find( key ) == m_fonts.end() )
    {
        Logger::Error( "Error finding font \"" + key + "\"", "FontManager::GetFont" );
        Logger::Out( "Error finding font \"" + key + "\"", "FontManager::GetFont", "fonts", true );
        return NULL;
    }
    return m_fonts[ key ];
}

//! Loads a font
/**
@param  path        The path of the font file to load
@param  size        The size of the font to load it as
@return             The loaded TTF_Font* object
*/
TTF_Font* FontManager::LoadFile( const std::string& path, int size )
{
    Logger::Out( "Load font \"" + path + "\"", "FontManager::LoadFile", "fonts" );
    TTF_Font* font = TTF_OpenFont( path.c_str(), size );
    return font;
}

}
