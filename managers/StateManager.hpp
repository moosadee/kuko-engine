// Kuko Framework - https://github.com/Rejcx/Kuko - Rachel J. Morris - MIT License
#ifndef _KUKO_STATEMANAGER
#define _KUKO_STATEMANAGER

#include "../states/IState.hpp"
#include "../base/Application.hpp"

#include <map>
#include <stack>
#include <string>

namespace kuko
{

//! Handles setting up and switching between game states
class StateManager
{
    public:
    StateManager();

    void Setup();
    void Cleanup();

    void AddState( const std::string& title, IState* ptrState );
    void PushState( const std::string& key );
    static void PopState();
    static std::string TopState();
    static std::string PopAndGetTop();
    static int GetStateStackSize();
    void UpdateCurrentState();
    void DrawCurrentState();
    bool IsDone();

    private:
    bool m_isDone;
    void CleanCurrentState();

    //! The key-value list of states in the game. States must inherit from IState
    std::map< std::string, IState* > m_lstStates;
    //! The stack of state names in view
    static std::stack< std::string > m_stateStack;
    //! The current state in use
    IState* m_ptrCurrentState;
};

}

#endif
