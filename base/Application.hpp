// Kuko Framework - https://github.com/Rejcx/Kuko - Rachel J. Morris - MIT License
#ifndef _KUKO_APPLICATION
#define _KUKO_APPLICATION

#include <SDL.h>

extern "C"
{
    #include "SDL.h"
}

#include <string>

/*
Handles initializing and tearing down SDL,
holds the renderer (might move later),
resizing/orienting the screen based on platform,
game timing,
input detection (might move later)
*/
namespace kuko
{
    //! Used for managing the frame rate
    struct Timer
    {
        Timer();
        int startTicks;
        int countedFrames;
        int fps;
        int ticksPerFrame;
        void Setup( int fps );
        void Start();
        void Update();
        void CapFrames();
        int GetTicks();
        void Reset();
    };

    //! Wraps the SDL initialization code and handles basics like the renderer and screen dimension information
    class Application
    {
        public:
        static bool Start( const std::string& winTitle, int screenWidth = 480, int screenHeight = 480, int defaultWidth = 480, int defaultHeight = 480, bool useVsync = false, bool fullscreen = false, bool borderless = false, bool fitToScreen = false );
        static void End();

        static int GetScreenWidth();
        static int GetScreenHeight();

        static int GetDefaultWidth();
        static int GetDefaultHeight();

        static float GetWidthRatio();
        static float GetHeightRatio();

        static void SetDefaultResolution( int width, int height );

        static SDL_Renderer* GetRenderer();
        static void BeginDraw();
        static void EndDraw();

        static void TimerStart();
        static void TimerUpdate();
        static float GetTimeStep();
        static void ResetStepTimer();

        static void ShowCursor( bool showCursor );

        private:
	//! The application window's width
        static int m_screenWidth;
	//! The application window's height
        static int m_screenHeight;

	//! The game's "actual" width
        static int m_defaultWidth;
	//! The game's "actual" height
        static int m_defaultHeight;

	//! The difference between the "actual" width and the window width
        static float m_widthRatio;
	//! The difference between the "actual" height and the window height
        static float m_heightRatio;

	//! The window object
        static SDL_Window* m_window;
	//! The renderer object
        static SDL_Renderer* m_renderer;

	//! The timer for FPS regulation
        static Timer m_timer;
	//! A timer for getting time info?
        static Timer m_stepTimer;

	//! Whether or not to use vsync
        static bool m_vsyncEnabled;
    };
}

#endif
