// Kuko Framework - https://github.com/Rejcx/Kuko - Rachel J. Morris - MIT License
#include "Application.hpp"

#include "../utilities/Logger.hpp"
#include "../utilities/StringUtil.hpp"

#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>

namespace kuko
{

int Application::m_screenWidth;
int Application::m_screenHeight;
int Application::m_defaultWidth;
int Application::m_defaultHeight;
float Application::m_widthRatio;
float Application::m_heightRatio;
bool Application::m_vsyncEnabled;

SDL_Window* Application::m_window = NULL;
SDL_Renderer* Application::m_renderer = NULL;
Timer Application::m_timer;
Timer Application::m_stepTimer;

//! Initializes the FPS timer
Timer::Timer()
{
    fps = 100;
    ticksPerFrame = 1000 / fps;
}

//! Sets up the target FPS
/**
@param	fps	The desired Frames Per Second in the application
*/
void Timer::Setup( int fps )
{
    countedFrames = 0;
}

//! Starts the timer
void Timer::Start()
{
    startTicks = SDL_GetTicks();
}

//! Updates the timer with the latest time
void Timer::Update()
{
    float averageFps = countedFrames / ( SDL_GetTicks() / 1000.0f );
    if ( countedFrames % 10000 == 0 )
    {
        Logger::Out( "Average FPS: " + StringUtil::FloatToString( averageFps ) );
    }

    ++countedFrames;
}

//! Caps the frames to regulate game speed
void Timer::CapFrames()
{
    int ticks = SDL_GetTicks();
    if ( ticks < ticksPerFrame )
    {
        SDL_Delay( ticksPerFrame - ticks );
    }
}

//! Get the amount of ticks that have passed since the timer began
/**
@return		The amount of ticks since the timer Start occurred
*/
int Timer::GetTicks()
{
    return SDL_GetTicks() - startTicks;
}

//! Reset the start time of the timer
void Timer::Reset()
{
    startTicks = SDL_GetTicks();
}

//! Starts the application timer
void Application::TimerStart()
{
    m_timer.Start();
}

//! Updates the application timer
void Application::TimerUpdate()
{
    m_timer.Update();
    if ( !m_vsyncEnabled )
    {
//        m_timer.CapFrames();
    }
}

//! Sets the application window's resolution
/**
@param	width	The width of the window, in pixels
@param	height	The height of the window, in pixels
*/
void Application::SetDefaultResolution( int width, int height )
{
    m_defaultWidth = width;
    m_defaultHeight = height;
}

//! Initializes the libraries and starts the program
/**
@param	winTitle	The text to display in the window's title bar
@param	screenWidth	The application window's width (default = 480)
@param	screenHeight	The application window's height (default = 480)
@param	defaultWidth	The "actual" width of the program, used for scaling
@param	defaultHeight	The "actual" height of the program, used for scaling
@param	useVsync	Whether to use vsync
@param	fullscreen	Whether to start the program in fullscreen
@param	borderless	Whether to start the program in borderless windowed
@param	fitToScreen	Whether to fit the program to the screen (default false)
*/
bool Application::Start( const std::string& winTitle, int screenWidth /* = 480 */, int screenHeight /* = 480 */, int defaultWidth, int defaultHeight, bool useVsync, bool fullscreen, bool borderless, bool fitToScreen /* = false */ )
{
    Logger::Out( "Title \"" + winTitle + "\", Screen: "
        + StringUtil::IntToString( screenWidth ) + "x" + StringUtil::IntToString( screenHeight )
        + ", Actual Resolution: "
        + StringUtil::IntToString( defaultWidth ) + "x" + StringUtil::IntToString( defaultHeight ),
        "Application::Start" );

    // Screen scaling is based on the default width/height
    SetDefaultResolution( defaultWidth, defaultHeight );

    m_timer.Setup( 60 );

    if ( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_AUDIO ) != 0 )
    {
        std::string error( SDL_GetError() );
        Logger::Error( "Error initializing SDL: " + error, "Application::Start" );
        return false;
    }

    if ( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
    {
        Logger::Error( "Error setting hint - Linear texture filtering not enabled!", "Application::Start" );
    }

    m_screenWidth = screenWidth;
    m_screenHeight = screenHeight;

    SDL_DisplayMode current;
    Logger::Out( "Display information", "Application::Start" );
    for( int i = 0; i < SDL_GetNumVideoDisplays(); ++i )
    {
        SDL_GetCurrentDisplayMode( i, &current );

        Logger::Out(
            "Display #" + StringUtil::IntToString( i ) +
            ": current display mode is " +
            StringUtil::IntToString( current.w )
            + " x " +
            StringUtil::IntToString( current.h )
            + " @ " +
            StringUtil::IntToString( current.refresh_rate ),
            "Application::Start"
            );
    }

    Uint32 windowFlags = SDL_WINDOW_SHOWN;
    if ( borderless )
    {
        windowFlags |= SDL_WINDOW_BORDERLESS;
    }
    if ( fullscreen )
    {
        windowFlags |= SDL_WINDOW_FULLSCREEN;
    }

    if ( fullscreen || fitToScreen )
    {
        m_screenWidth = current.w;
        m_screenHeight = current.h;
    }

    m_window = SDL_CreateWindow( winTitle.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, m_screenWidth, m_screenHeight, windowFlags );

    if ( m_window == NULL )
    {
        std::string error( SDL_GetError() );
        Logger::Error( "Error creating window: " + error, "Application::Start" );
        return false;
    }

    Uint32 renderFlags = SDL_RENDERER_ACCELERATED;
    m_vsyncEnabled = useVsync;
    if ( useVsync )
    {
        Logger::Out( "Use VSYNC", "Application::Start" );
        renderFlags |= SDL_RENDERER_PRESENTVSYNC;
    }
    else
    {
        Logger::Out( "Do not use VSYNC", "Application::Start" );
    }

    m_renderer = SDL_CreateRenderer( m_window, -1, renderFlags );

    if ( m_renderer == NULL )
    {
        std::string error( SDL_GetError() );
        Logger::Error( "Error initializing renderer: " + error, "Application::Start" );
        return false;
    }
    SDL_SetRenderDrawColor( m_renderer, 0xFF, 0xFF, 0xFF, 0xFF );

    int imgFlags = IMG_INIT_PNG;
    if ( !( IMG_Init( imgFlags ) & imgFlags ) )
    {
        std::string error( IMG_GetError() );
        Logger::Error( "Error initializing SDL_Image: " + error, "Application::Start" );
        return false;
    }

    if( TTF_Init() == -1 )
    {
        std::string error( TTF_GetError() );
        Logger::Error( "Error initializing SDL_TTF: " + error, "Application::Start" );
        return false;
    }

    if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )
    {
        std::string error( Mix_GetError() );
        Logger::Error( "Error initializing SDL_Mixer: " + error, "Application::Start" );
        return false;
    }

    m_widthRatio = float( m_screenWidth ) / float( m_defaultWidth );
    m_heightRatio = float( m_screenHeight ) / float( m_defaultHeight );

    Logger::Out( "Renderer scale ratio: " + StringUtil::FloatToString( m_widthRatio ) + "x" + StringUtil::FloatToString( m_heightRatio ), "Application::Start" );

    //SDL_RenderSetScale( m_renderer, 0.5, 0.5 );
    SDL_RenderSetScale( m_renderer, m_widthRatio, m_heightRatio );

    return true;
}

//! Clean up the application
void Application::End()
{
    Logger::Out( "Cleaning up application!", "Application::End" );

    if ( m_renderer != NULL )
    {
        SDL_DestroyRenderer( m_renderer );
        m_renderer = NULL;
    }

    if ( m_window != NULL )
    {
        SDL_DestroyWindow( m_window );
        m_window = NULL;
    }

    SDL_Quit();
}

//! Get the application window width
/**
@return		The application window's width
*/
int Application::GetScreenWidth()
{
    return m_screenWidth;
}

//! Get the application window height
/**
@return		The application window's height
*/
int Application::GetScreenHeight()
{
    return m_screenHeight;
}

//! Get the game's "actual" width (used for scaling)
/**
@return		The game's "actual" width
*/
int Application::GetDefaultWidth()
{
    return m_defaultWidth;
}

//! Get the game's "actual" height (used for scaling)
/**
@return		The game's "actual" height
*/
int Application::GetDefaultHeight()
{
    return m_defaultHeight;
}

//! Get the width ratio (between window size / actual size)
/**
@return		The width ratio
*/
float Application::GetWidthRatio()
{
    return m_widthRatio;
}

//! Get the height ratio (between the window size / actual size)
/**
@return		The height ratio
*/
float Application::GetHeightRatio()
{
    return m_heightRatio;
}

//! Get the SDL Renderer
/**
@return		The SDL renderer for the application
*/
SDL_Renderer* Application::GetRenderer()
{
    return m_renderer;
}

//! Iinitialize drawing
void Application::BeginDraw()
{
    SDL_SetRenderDrawColor( m_renderer, 0x33, 0x33, 0x33, 0xFF );
    SDL_RenderClear( m_renderer );
}

//! Finish drawing
void Application::EndDraw()
{
    SDL_RenderPresent( m_renderer );
}

//! Get the time step
/**
@return		The time step - amount of ticks divided by 1000
*/
float Application::GetTimeStep()
{
    return m_stepTimer.GetTicks() / 1000.0f;
}

//! Reset the step timer
void Application::ResetStepTimer()
{
    m_stepTimer.Reset();
}

//! Toggles showing the hardware cursor or not. If you have a drawn cursor in game, disable this.
/**
@param	showCursor	Whether to show the operating system's cursur
*/
void Application::ShowCursor( bool showCursor )
{
    if ( showCursor )
    {
        SDL_ShowCursor( SDL_ENABLE );
    }
    else
    {
        SDL_ShowCursor( SDL_DISABLE );
    }
}

}
