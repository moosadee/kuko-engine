// Kuko Framework - https://github.com/Rejcx/Kuko - Rachel J. Morris - MIT License
#ifndef _KUKO_SPRITE
#define _KUKO_SPRITE

#include <SDL.h>
#include <SDL_image.h>

#include <string>
#include <iostream>

#include "../utilities/Logger.hpp"
#include "PositionRect.hpp"

namespace kuko
{

//! Sprite class that wraps SDL_Texture functionality
class Sprite
{
    public:
    Sprite();
    Sprite( const Sprite& copyme );
    ~Sprite();

    void SetTexture( SDL_Texture* ptrTexture );
    SDL_Texture* GetTexture();
    void SetPosition( kuko::FloatRect pos );
    void SetPosition( float x, float y );
    FloatRect GetPosition();
    void SetFrame( IntRect fr );
    IntRect GetFrame();
    void SetRotation( float angle );
    float GetRotation();
    bool IsClicked( int x, int y );
    void SetAlpha( Uint8 value );
    Uint8 GetAlpha();

    void Draw();
    void DrawWithOffset( const kuko::IntRect& offset );

    //! Main texture
    SDL_Texture*        texture;

    //! Frame to display (portion of the sprite sheet)
    IntRect             frame;
    //! Rotation angle
    float               angle;
    //! Position and dimensions of the sprite
    FloatRect           position;
    //! Whether to horizontally flip the sprite
    bool                isFlipped;

    private:
    //! Alternative texture
    SDL_Texture* m_altTexture;
};

}

#endif
