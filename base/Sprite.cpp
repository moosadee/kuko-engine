// Kuko Framework - https://github.com/Rejcx/Kuko - Rachel J. Morris - MIT License
#include "Sprite.hpp"

#include "../base/Application.hpp"
#include "../utilities/StringUtil.hpp"
#include "../managers/ImageManager.hpp"

namespace kuko
{

//! Initialize the Sprite with defaults
Sprite::Sprite()
{
    texture = NULL;
    frame.x = frame.y = 0;
    frame.w = frame.h = 16;

    position.x = position.y = 0;
    position.w = position.h = 16;
    isFlipped = false;
    angle = 0.0f;
}

//! Initialize the Sprite as a copy
Sprite::Sprite( const Sprite& copyme )
{
    texture = copyme.texture;
    frame = copyme.frame;
    position = copyme.position;
    isFlipped = copyme.isFlipped;
    angle = copyme.angle;
}

//! Destroy the texture
Sprite::~Sprite()
{
//    SDL_DestroyTexture( texture ); // Should be handled by the image manager!
    texture = NULL;
}

//! Returns whether the Sprite is being clicked based on the mouse (x, y) coordinates
/**
@param	x	The mouse's x coordinate
@param	y	The mouse's y coordinate
@return		True if the mouse's coordinates are within the Sprite's region, False if not.
*/
bool Sprite::IsClicked( int x, int y )
{
    return ( position.x <= x && position.x + position.w > x && position.y <= y && position.y + position.h > y );
}

//! Set the Sprite's position and dimensions
/**
@param	pos	The FloatRect that stores the intended (x, y) position and width x height information for the Sprite
*/
void Sprite::SetPosition( kuko::FloatRect pos )
{
    position.x = pos.x;
    position.y = pos.y;
    position.w = pos.w;
    position.h = pos.h;
}

//! Set the (x, y) position of the Sprite
/**
@param	x	The x coordinate to move the Sprite to
@param	y	The y coordinate to move the Sprite to
*/
void Sprite::SetPosition( float x, float y )
{
    position.x = x;
    position.y = y;
}

//! Get the position of the sprite
/**
@return		The current position FloatRect of the Sprite
*/
FloatRect Sprite::GetPosition()
{
    return position;
}

//! Set the Sprite's frame (what's displayed from the spritesheet)
/**
@param	fr	The IntRect of the position and dimensions of the frame on the spritesheet to be drawn
*/
void Sprite::SetFrame( IntRect fr )
{
    frame = fr;
}

//! Get the Sprite's frame rect (what's displayed from the spritesheet)
/**
@return		An IntRect of the region on the spritesheet to be drawn
*/
IntRect Sprite::GetFrame()
{
    return frame;
}

//! Set the sprite's rotation angle
/**
@param	angle		The rotation angle of the Sprite
*/
void Sprite::SetRotation( float angle )
{
    this->angle = angle;
}

//! Get the current rotation of the sprite
/**
@return		The current sprite rotation angle
*/
float Sprite::GetRotation()
{
    return angle;
}

//! Set the texture of the Sprite
/**
@param ptrTexture	Pointer to the texture to be stored
*/
void Sprite::SetTexture( SDL_Texture* ptrTexture )
{
    texture = ptrTexture;//LoadFile( path );
    position.x = position.y = 0;
    frame.x = frame.y = 0;

    int w, h;
    SDL_QueryTexture( texture, NULL, NULL, &w, &h );
    position.w = frame.w = w;
    position.h = frame.h = h;
}

//! Get the SDL_Texture stored in this sprite
/**
@return		A pointer to the internal SDL_Texture
*/
SDL_Texture* Sprite::GetTexture()
{
    return texture;
}

//! Set the alpha of the sprite
/**
@param	value		The Uint8 value of transparency for the Sprite
*/
void Sprite::SetAlpha( Uint8 value )
{
    SDL_SetTextureAlphaMod( texture, value );
}

//! Get the current alpha of the sprite
/**
@return	The Uint8 value of the transparency of the Sprite
*/
Uint8 Sprite::GetAlpha()
{
    Uint8 alpha;
    SDL_GetTextureAlphaMod( texture, &alpha );
    return alpha;
}

//! Draw the sprite to the screen, with the stored position, frame, rotation, and flip information
void Sprite::Draw()
{
    SDL_Rect rectPos = position.ToSDLRect();
    SDL_Rect rectFrame = frame.ToSDLRect();
    SDL_RendererFlip flipped = ( isFlipped ) ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE;
    ImageManager::Draw( texture, &rectPos, &rectFrame, flipped, angle );
}

void Sprite::DrawWithOffset( const kuko::IntRect& offset )
{
//    Logger::Error( "Error: This function is not implemented!", "Sprite::Draw( const kuko::IntRect& offset )" );
    Sprite clone( *this );

    // Offset its position by the offset
    clone.position.x += offset.x;
    clone.position.y += offset.y;

    clone.Draw();
}

}
